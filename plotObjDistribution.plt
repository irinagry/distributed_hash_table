clear
reset
set key off
set border 3
set terminal pdf

set xlabel "Server"
set ylabel "#(Objects)"
set output "objDistribution.pdf"

# Each bar is half the (visual) width of its x-range.
set boxwidth 0.05 absolute
set style fill solid 1.0 noborder

# Plotting
plot "object_distribution" using ($1+1):(1) smooth frequency with boxes

