#!/bin/bash

INTERF=h$1-eth0
echo $INTERF

ifconfig $INTERF mtu 1500
ethtool -K $INTERF gso off; ethtool -K $INTERF tso off
tc qdisc add dev $INTERF root handle 1: tbf rate 100Mbit burst 10000 latency 10ms
tc qdisc add dev $INTERF parent 1:1 handle 10: netem delay 5ms

memcached -u mininet 
