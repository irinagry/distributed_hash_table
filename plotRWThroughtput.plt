clear
reset
set terminal pdf

set xlabel "object size"
set ylabel "time"
set output "rwThroughput.pdf"

# Plotting
plot "read_throughput" using 1:2 title "Read" with linespoints, \
     "write_throughput" using 1:2 title "Write" with lines
