CC = gcc
CFLAGS = -lmemcached -lcrypto
SRC = challenge4.c
EXE = dht
 
all:
	$(CC) -o $(EXE) $(SRC) $(CFLAGS)
 
.PHONY : clean
clean :
	rm -f $(EXE) *~
