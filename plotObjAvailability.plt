clear
reset
set key off
set border 3
set terminal pdf

set xlabel "#(down servers)"
set ylabel "#(available objects)"
set output "objAvailability.pdf"

# Each bar is half the (visual) width of its x-range.
set boxwidth 0.05 absolute
set style fill solid 1.0 noborder

# Plotting
plot "object_availability_0" using ($1):(1) smooth frequency with boxes, \
	 "object_availability_1" using ($1+1):(1) smooth frequency with boxes, \
	 "object_availability_2" using ($1+2):(1) smooth frequency with boxes, \
	 "object_availability_3" using ($1+3):(1) smooth frequency with boxes, \
	 "object_availability_4" using ($1+4):(1) smooth frequency with boxes, \
	 "object_availability_5" using ($1+5):(1) smooth frequency with boxes, \
	 "object_availability_6" using ($1+6):(1) smooth frequency with boxes


