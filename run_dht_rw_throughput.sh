#!/bin/bash

if [ $# -eq 0 ]
  then
    echo -e "No arguments supplied."
    echo -e ""
    echo -e "Usage: "
    echo -e "./run_dht_rw_throughput.sh write"
    echo -e "OR"
    echo -e "./run_dht_rw_throughput.sh read"
    exit 1
fi

# Write objects of different sizes
if [ "$1" == "write" ]
  then
	echo "" > write_throughput
	for i in 100000 200000 300000 400000 500000 600000 700000 800000 900000 1000000
	do
	
		./dht put $i "value_$j" >> write_throughput
	
	done
fi

# Read objects of different sizes
if [ "$1" == "read" ]
  then
	echo "" > read_throughput
	for i in 100000 200000 300000 400000 500000 600000 700000 800000 900000 1000000
	do	
		
		./dht get $i >> read_throughput
		
	done
fi
