#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/timeb.h>
#include <time.h>
#include <libmemcached/memcached.h>
#include <openssl/md5.h>
#include <openssl/sha.h>

//#define DEBUG 
#define INFO
//#define OBJ_DSTR
//#define OBJ_AVAIL
//#define RW_THRG

double elapse(struct timeb *before, struct timeb *after)
{
	double t;
	t = (after->time*1000 + after->millitm) - (before->time*1000 + before->millitm);
	t /= 1000;
	return t;
}

char *str2md5(const char *str, int length) {
	int n;
	MD5_CTX c;
	unsigned char digest[16];
	char *out = (char*)malloc(33);

	MD5_Init(&c);

	while (length > 0) {
		if (length > 512) {
			MD5_Update(&c, str, 512);
		} else {
			MD5_Update(&c, str, length);
		}
		length -= 512;
		str += 512;
	}

	MD5_Final(digest, &c);

	for (n = 0; n < 16; ++n) {
		snprintf(&(out[n*2]), 16*2, "%02x", (unsigned int)digest[n]);
	}

	return out;
}

char *str2sha(const char *str, int length) {
	int n;
	unsigned char digest[20];
	char *out = (char*)malloc(41);
	
	SHA1(str, strlen(str), digest);
	
	for (n = 0; n < 20; ++n) {
		snprintf(&(out[n*2]), 20*2, "%02x", (unsigned int)digest[n]);
	}
	
	return out;
}

// Method that returns id of server to put info on
int consistent_hashing(char *info, int no_servers, int replication) {
	long number;
	
	if (replication) {
		char hash[16];
		strncpy(hash, str2sha(info, strlen(info)), 15);
		hash[15] = 0;
		number = (long) strtol(hash, NULL, 16);
		//printf("CH sha: %s\n", hash);
		//printf("CH sha: %ld\n", number);
	} else {
		char hash[16];
		strncpy(hash, str2md5(info, strlen(info)), 15);
		hash[15] = 0;
		number = (long) strtol(hash, NULL, 16);
		//printf("CH md5: %s %d\n", hash, strlen(hash));
		//printf("CH md5: %ld\n", number);
	}
	
	return (number % no_servers);
}

int main(int argc, char *argv[])
{
	int sz, i, count = 2, server, server_rep;
	
	int key;
	char value[50];
	
	if (argc < 3) {
		printf("\nUsage: ./dht put <key> <value>\n");
		printf("or\n");
		printf("Usage: ./dht get <key>\n");
	} 
	
	else {
		
		// Get list of servers and save them in an array
		char server_list[20][16], line[16];
		int no_servers = 0;
		
		FILE *fr = fopen("server_list", "rt"); 
		
		if (fr == NULL) {
			printf("Error opening file!\n");
			exit(1);
		}
		
		while(fgets(line, 16, fr) != NULL)
		{
			char *p = line; p[strlen(line)-1] = 0;
			strcpy(server_list[no_servers], p);
			//printf ("-%s-\n", server_list[no_servers]);
			no_servers++;
		}
		fclose(fr);
		
		// PUT OPERATION
		if(!strcmp(argv[1], "put")) {
			#ifdef DEBUG
			printf("--PUT OPERATION--\n");
			#endif
			
			// Get passed arguments
			key = atoi(argv[2]);
			strcpy(value, argv[3]);
			
			// Let's see where we put this key
			char key_to_add[20]; sprintf(key_to_add, "key_%06d", key);
			int strlen_value = 0;
			server = consistent_hashing(key_to_add, no_servers, 0);
			server_rep = consistent_hashing(key_to_add, no_servers, 1);
			if (server_rep == server) {
				if ((server_rep + 1) == no_servers)
					server_rep = 1;
				else
					server_rep = server + 1;
			}
			//printf("CH no rep returns: %d\n", server);
			//printf("CH    rep returns: %d\n", server_rep);

			//return(EXIT_SUCCESS);

			// ///////////////////////////
			// Memcached Create Context
			memcached_st *memc = memcached_create(NULL);
			memcached_st *memc_rep = memcached_create(NULL);

			memcached_return_t error = memcached_server_add(memc, server_list[server], 0 /* use default port*/);
			memcached_return_t error_rep = memcached_server_add(memc_rep, server_list[server_rep], 0 /* use default port*/);
			
			// Addind server
			#ifdef DEBUG
			if (error == MEMCACHED_SUCCESS)
				fprintf(stderr,"Added %s server successfully\n", server_list[server]);
			else
				fprintf(stderr,"Couldn't add %s server: %s\n", server_list[server], memcached_strerror(memc, error));
			#endif
			
			// Addind replication server
			#ifdef DEBUG
			if (error_rep == MEMCACHED_SUCCESS)
				fprintf(stderr,"Added %s server successfully\n", server_list[server_rep]);
			else
				fprintf(stderr,"Couldn't add %s server: %s\n", server_list[server_rep], memcached_strerror(memc_rep, error_rep));
			#endif
			// ///////////////////////////

			// ADDING VALUE to servers
			strlen_value = strlen(value);
				#ifdef RW_THRG
				struct timeb before, after, before2;
				double t;
				strlen_value = key;
				char *value = malloc(strlen_value*sizeof(char));
				ftime(&before);
				#endif
			
			// Server
			
			memcached_return_t rc = memcached_set(memc, key_to_add, strlen(key_to_add), value, strlen_value, (time_t)0, (uint32_t)0);
				
				#ifdef RW_THRG
				ftime(&after);
				t = elapse(&before, &after);
				// Printing the elapsed time
				printf("%d %lf\n", strlen_value, t);
				#endif
			
			// Replication Server
			memcached_return_t rc_rep = memcached_set(memc_rep, key_to_add, strlen(key_to_add), value, strlen_value, (time_t)0, (uint32_t)0);

			// Verifying
			if (rc == MEMCACHED_SUCCESS) {
				#ifdef INFO
				fprintf(stderr, "Key %s stored successfully on %s\n", key_to_add, server_list[server]);
				#endif
				// Print Object distribution to stdout:
				#ifdef OBJ_DSTR
				printf("%d\n", server);
				#endif
			}
			else {
				#ifdef INFO
				fprintf(stderr, "Couldn't store key: %s\n", memcached_strerror(memc, rc));
				#endif
			}
				
			// Putting on replication server as well
			if (rc_rep == MEMCACHED_SUCCESS) {
				#ifdef INFO
				fprintf(stderr, "Key %s stored successfully on %s\n", key_to_add, server_list[server_rep]);
				#endif
				// Print Object distribution to stdout:
				#ifdef OBJ_DSTR
				printf("%d\n", server_rep);
				#endif
			}
			else {
				#ifdef INFO
				fprintf(stderr, "Couldn't store key: %s\n", memcached_strerror(memc_rep, rc));
				#endif
			}


			// Free Context
			memcached_free(memc);
			memcached_free(memc_rep);
			
		} else { // GET OPERATION
			#ifdef DEBUG
			printf("--GET OPERATION--\n");
			#endif
			
			// Get passed arguments
			key = atoi(argv[2]);
			
			// Let's see where we get this key from
			char key_to_get[20]; sprintf(key_to_get, "key_%06d", key);
			server = consistent_hashing(key_to_get, no_servers, 0);
			server_rep = consistent_hashing(key_to_get, no_servers, 1);
			if (server_rep == server) {
				if ((server_rep + 1) == no_servers)
					server_rep = 1;
				else
					server_rep = server + 1;
			}
			//printf("CH no rep returns: %d\n", server);
			//printf("CH    rep returns: %d\n", server_rep);

			//exit(EXIT_SUCCESS);
			
			// ///////////////////////////
			// Memcached Create Context
			memcached_st *memc = memcached_create(NULL);
			memcached_st *memc_rep = memcached_create(NULL);

			memcached_return_t error = memcached_server_add(memc, server_list[server], 0 /* use default port*/);
			memcached_return_t error_rep = memcached_server_add(memc_rep, server_list[server_rep], 0 /* use default port*/);
			
			// Addind server
			#ifdef DEBUG
			if (error == MEMCACHED_SUCCESS)
				fprintf(stderr,"Added %s server successfully\n", server_list[server]);
			else
				fprintf(stderr,"Couldn't add %s server: %s\n", server_list[server], memcached_strerror(memc, error));
			#endif
			
			// Addind replication server
			#ifdef DEBUG
			if (error_rep == MEMCACHED_SUCCESS)
				fprintf(stderr,"Added %s server successfully\n", server_list[server_rep]);
			else
				fprintf(stderr,"Couldn't add %s server: %s\n", server_list[server_rep], memcached_strerror(memc_rep, error_rep));
			#endif
			// ///////////////////////////
			
			
			// RETRIEVING VALUE from servers
			memcached_return_t rc, rc_rep;
			size_t value_length;
			char *value_to_get, *value_to_get_rep;
			
				#ifdef RW_THRG
				struct timeb before, after, before2;
				double t;
				ftime(&before);
				
				#endif
				
			// Server
			value_to_get = memcached_get(memc, key_to_get, strlen(key_to_get), &value_length, 0, &rc);
				
				#ifdef RW_THRG
				ftime(&after);
				t = elapse(&before, &after);
				// Printing the elapsed time
				printf("%d %lf\n", (int)value_length, t);
				#endif
			
			// Replication Server
			value_to_get_rep = memcached_get(memc_rep, key_to_get, strlen(key_to_get), &value_length, 0, &rc_rep);
				
			// Verifying success
			if (rc == MEMCACHED_SUCCESS) {
				#ifdef INFO
				fprintf(stderr, "Get of %s returned %s on %s\n", key_to_get, value_to_get, server_list[server]);
				#endif
				
				#ifdef OBJ_AVAIL
				printf("%d\n", 0);
				#endif
			}
			else {
				// Trying on replication server
				if (rc_rep == MEMCACHED_SUCCESS) {
					#ifdef INFO
					fprintf(stderr, "Get of %s returned %s on %s\n", key_to_get, value_to_get, server_list[server_rep]);
					#endif
										
					#ifdef OBJ_AVAIL
					printf("%d\n", 0);
					#endif
				}
				else {
					#ifdef INFO
					fprintf(stderr, "Couldn't get value of key: %s\n", memcached_strerror(memc_rep, rc_rep));
					#endif
				}
			}
				
			
			// Free Context
			memcached_free(memc);
			memcached_free(memc_rep);
		}

	}


  return 0;
}
