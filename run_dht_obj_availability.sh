#!/bin/bash

if [ $# -eq 0 ]
  then
    echo -e "No arguments supplied."
    echo -e ""
    echo -e "Usage: "
    echo -e "./run_dht_obj_availability.sh put"
    echo -e "OR"
    echo -e "./run_dht_obj_availability.sh get <servers_down_number>"
    echo -e ""
    echo -e "<servers_down_number> refers to the number of down servers you are running your test on."
    exit 1
fi

# Do 100 put
if [ "$1" == "put" ]
  then
	for i in {1..100}
	do
		./dht put $i "value_$i" 
	done
fi

# Do 100 get with 0..5 hosts down
if [ "$1" == "get" ]
  then
	echo "" > object_availability_$2
	for i in {1..100}
	do
		./dht get $i >> object_availability_$2
	done
fi


